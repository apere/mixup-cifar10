#!/usr/bin/env python
"""
This file contains an example of a runaway scheduler that manages executions to be performed with an 
xlsx file. The parameters are read from the file, and when an execution is over, the result is written 
in the ods file. It supports automatic rescheduling of failed executions. The xslx file is automatically 
updated with results during the execution.

If you want to modify this file to do different things, you just need to modify the `Scheduler` 
object, and in particular the `handle_` prefixed methods.
"""

import json 
import sys
import argparse
import pandas
import numpy
import traceback


def log(inpt):
    sys.stderr.write(f"Scheduler: {inpt}\n")


def get_arguments():
    parser = argparse.ArgumentParser(description='A runaway scheduler written in python')
    parser.add_argument('--file', help='Path to an xlsx file containing the parameters to execute.')
    parser.add_argument('--reschedule', action="store_true", help="Activats automatic rescheduling of failed executions.")
    return vars(parser.parse_args())


def main(scheduler):
    while True:
        inpt = json.loads(input())
        if "GET_PARAMETERS_REQUEST" in inpt.keys():
            scheduler.handle_parameter_request(**inpt["GET_PARAMETERS_REQUEST"])    
        elif "RECORD_OUTPUT_REQUEST" in inpt.keys():
            scheduler.handle_record_output_request(**inpt["RECORD_OUTPUT_REQUEST"])
        elif "SHUTDOWN_REQUEST" in inpt.keys():
            break
        else:
            raise Exception(f"Protocol mismatch. Received unexpected message: {inpt}.")
    scheduler.handle_shutdown_request()


class Scheduler(object):

    def __init__(self, file, reschedule):
        self.table = pandas.read_excel(file)
        self.table["spawned"] = self.table["uuid"].notna()
        self.file = file
        self.reschedule = reschedule
        
    def _get_execution(self, uuid):
        return self.table.loc[lambda s: s["uuid"] == uuid].iloc[0]

    def _are_all_performed(self):
        return all(self.table["ecode"].notna())

    def _reset_execution(self, index):
        self.table.loc[index, "uuid"] = numpy.nan
        self.table.loc[index, "spawned"] = False

    def _init_execution(self, index, uuid):
        self.table.loc[index, "spawned"] = True
        self.table.loc[index, "uuid"] = uuid

    def _finish_execution(self, index, parameters, stdout, stderr, ecode, path, features):
        self.table.loc[index, "parameters"] = parameters
        self.table.loc[index, "stdout"] = stdout
        self.table.loc[index, "stderr"] = stderr
        self.table.loc[index, "ecode"] = ecode
        self.table.loc[index, "path"] = path
        self.table.loc[index, "features"] = features

    def _get_remaining(self):
        return self.table[self.table["spawned"] == False]

    def _drop_outputs(self, serie):
        return serie.drop(["spawned", "uuid", "parameters", "stdout", "stderr", "ecode", "path", "features"])

    def _save_table(self):
        with pandas.ExcelWriter(self.file) as writer:
            self.table.drop(labels="spawned", axis=1).to_excel(writer, engine="xlsxwriter", index=False)

    def _pop_param(self, uuid):
        params = self._get_remaining()
        if len(params) == 0:
            return None
        else:
            serie = params.iloc[0]
            self._init_execution(serie.name, uuid)
            return self._generate_param_string(self._drop_outputs(serie).to_dict())

    def _generate_param_string(self, record_dict):
        string = ""
        for key, item in record_dict.items():
            if type(item) is numpy.bool_ :
                if item:
                    string += f"{key} "
            else:
                string += f"{key}={item} "
        return string

    def _return_error_response(self, message):
        return print(json.dumps({"ERROR_RESPONSE": {"message": message}}))
    
    def _return_not_ready_response(self):
        return print(json.dumps({"NOT_READY_RESPONSE": {}}))

    def _return_record_output_response(self):
        return print(json.dumps({"RECORD_OUTPUT_RESPONSE": {}}))

    def _return_shutdown_response(self):
        return print(json.dumps({"SHUTDOWN_RESPONSE": {}}))
    
    def _return_get_parameters_response(self, parameters):
        return print(json.dumps({"GET_PARAMETERS_RESPONSE": {"parameters": parameters}}))
    
    def handle_parameter_request(self, uuid):
        try:
            next_parameters = self._pop_param(uuid)
            if next_parameters is None:
                if self._are_all_performed():
                    self._return_error_response("All executions were processed.") 
                else:
                    self._return_not_ready_response()
            else:
                self._return_get_parameters_response(next_parameters)
        except Exception as e:
            traceback.print_tb(e.__traceback__)
            self._return_error_response(f"Exception occurred: {e}")

    def handle_record_output_request(self, uuid, parameters, stdout, stderr, ecode, path, features):
        try:
            if ecode != 0 and self.reschedule:
                log(f"Execution {uuid} failed. Rescheduling ...")
                self._reset_execution(self._get_execution(uuid).name)
            else:
                self._finish_execution(self._get_execution(uuid).name, parameters, stdout, stderr, ecode, path, features)
            self._return_record_output_response()
            self._save_table()
        except Exception as e:
            traceback.print_tb(e.__traceback__)
            self._return_error_response(f"Exception occurred: {e}")

    def handle_shutdown_request(self):
        try:
            self._save_table()
            self._return_shutdown_response()
        except Exception as e:
            traceback.print_tb(e.__traceback__)
            self._return_error_response(f"Exception occurred: {e}")


if __name__ == "__main__":

    args = get_arguments()
    scheduler = Scheduler(**args)
    main(scheduler)
